const express = require("express");
const app = express();
const morgan = require("morgan");

//SETTINGS
app.set("port", process.env.PORT || 3000); //CREA VARIABLES GLOBALES CON EXPRESS
app.set("json spaces", 2);

//INICIA MORGAN(PERMITE VER POR CONSOLA LO QUE LLEGA AL SERVIDOR)
app.use(morgan("combined"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//RUTAS
app.use(require("./routes/index"));
app.use("/api/movies", require("./routes/movies"));

//INICIAL EL SERVIDOR
app.listen(app.get("port"), () => {
  console.log("Server on port 3000");
});
